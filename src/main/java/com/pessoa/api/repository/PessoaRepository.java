package com.pessoa.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pessoa.api.models.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Integer> {
	
	Pessoa findById(int Id);

}
